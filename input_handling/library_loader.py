import os.path
import pickle

import numpy as np

from input_handling.sentence_embedder import create_sentence_vector


class VectorSingleton(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(VectorSingleton, cls).__new__(cls)
            cls.instance.vectors_dict = {}
            cls.instance.phrase_vector_values_dict = {}
        return cls.instance

    # TODO path from env variable
    # TODO this needs to be loaded from database of more efficiently
    def load_vectors(self):
        if not os.path.exists('data/vectors.pkl'):
            # TODO from env variable
            with open('data/vectors.csv', 'r') as file:
                for line in file:
                    values = line.split()
                    word = values[0]
                    vector = np.asarray(values[1:], dtype='float32')
                    self.vectors_dict[word] = vector
            with open('data/vectors.pkl', 'wb') as file:
                pickle.dump(self.vectors_dict, file)
        else:
            with open('data/vectors.pkl', 'rb') as file:
                self.vectors_dict = pickle.load(file)

    def load_phrase_vectors(self):
        with open('data/phrases.csv', 'r', encoding="latin-1") as file:
            phrases = [line.strip() for line in file]
            for phrase in phrases:
                phrase_vector = create_sentence_vector(phrase, self.vectors_dict)
                if phrase_vector is not None:
                    self.phrase_vector_values_dict[phrase] = phrase_vector


# Usage
vectors_singleton = VectorSingleton()
vectors_singleton.load_vectors()
vectors_singleton.load_phrase_vectors()
vectors_dict = vectors_singleton.vectors_dict
phrase_vector_dict = vectors_singleton.phrase_vector_values_dict


