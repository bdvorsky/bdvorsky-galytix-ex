import numpy as np


def create_sentence_vector(sentence, vectors_dict):
    words = sentence.split()
    word_embeddings = [vectors_dict.get(word) for word in words]
    word_embeddings = [embedding for embedding in word_embeddings if embedding is not None]
    # TODO exception
    if not word_embeddings:
        return None
    # Sum the embeddings and normalize the result
    sentence_vector = np.sum(word_embeddings, axis=0)
    sentence_vector /= np.linalg.norm(sentence_vector)  # Normalize to unit length
    return sentence_vector
