# bdvorsky-galytix-ex



## Getting started

To run the project, make sure you have input files:
- phrases.csv
- vectors.csv

`<application>/data` folder.

Then you can start the application with:

`python3 main.py --string "This is a nice sentence"`
for on-the-fly processing

or
`python3 main.py --batch`
for batch mode


## Docker run

You can also run the app with Docker as a function. Build the image first:
`docker build -t bdvorsky-galytix-ex .`

Then run the container:

`docker run -v <input_data_location>:/app/data bdvorsk/glx python3 main.py --batch`

for batch use cases. Output matrix will be created in the same folder as the input data.

`docker run -v <input_data_location>:/app/data bdvorsk/glx python3 main.py --string "Hello hello hello"`

for on-the-fly processing. Output will be printed to the console.


## Summary of my work

Although I was able to get the functional code up and running fairly quickly, I got stuck in the application design. Subsequently, I got completely bogged down in details mainly caused by the impurity of my code and a poor application design, which consumed a lot of my time that could have been used to complete the project to my satisfaction. This is a hastily created program, and I am quite unhappy with it. I didn't have time for:

- setting up logging (even though it would only take a few minutes)
- tests
- input validation
- error handling
- proper application setup
- proper use of OOP principles
- optimization (it would have been interesting to set up a database - I attempted it for a while, but after losing 10 minutes on it, I abandoned it due to lack of time)
- use of distributed frameworks (I planned to try it in Spark)
- comments, to-dos, etc.
- Docker deployment is really as simple as I managed to do it in 5 minutes
- various inputs (different phrases.csv)
- transformation of input binary file to CSV

During the completion of this task, I realized that while I am capable of handling such a task, the main focus of my previous work is mainly scripting and solving various problems. Nevertheless, I have little experience in creating clean, production-ready applications. Even though I know what to look for and roughly what to do, I am unable to complete such a task within the specified time frame. I estimate that fulfilling the requirements to everyone's satisfaction with a significant portion of the bonuses could take me around 8 hours instead of the designated 4. Thank you for this exercise, as it has given me insight into what to focus on, where my weaknesses are, and what I need to improve. I evaluate my performance extremely critically, but once again, thank you for the opportunity.