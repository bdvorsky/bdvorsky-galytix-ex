# Use the official Python image
FROM python:latest

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any dependencies your application needs
RUN pip install --no-cache-dir -r requirements.txt



