import numpy as np


# TODO choose distance metric from the input
def calculate_distance_to_default_phrases(phrase_vector, phrase_vector_values_dict, distance_metric='cosine'):
    # Calculate the distance between the phrase vector and all other phrase vectors
    distances = {}
    for phrase, vector in phrase_vector_values_dict.items():
        if distance_metric == 'cosine':
            distance = 1 - np.dot(phrase_vector, vector)
        elif distance_metric == 'euclidean':
            distance = np.linalg.norm(phrase_vector - vector)
        # TODO prepared for choosing distance metric from the input
        else:
            raise ValueError(f"Unsupported distance metric '{distance_metric}'")
        distances[phrase] = distance

    return distances


# TODO choose distance metric from the input
def calculate_distance_of_vector_to_vector(first_phrase_vector, second_phrase_vector, distance_metric='cosine'):
    if distance_metric == 'cosine':
        distance = 1 - np.dot(first_phrase_vector, second_phrase_vector)
    elif distance_metric == 'euclidean':
        distance = np.linalg.norm(first_phrase_vector - second_phrase_vector)
    # TODO prepared for choosing distance metric from the input
    else:
        raise ValueError(f"Unsupported distance metric '{distance_metric}'")

    return distance


