import numpy as np
import pickle
from input_handling.sentence_embedder import create_sentence_vector
from processing.similarity import calculate_distance_to_default_phrases, calculate_distance_of_vector_to_vector
from input_handling.library_loader import phrase_vector_dict, vectors_dict


def process_on_the_fly_mode(input_string):
    sentence_vector = create_sentence_vector(input_string, vectors_dict)
    distances_dict = calculate_distance_to_default_phrases(sentence_vector, phrase_vector_dict)
    minimal_distance_key = min(distances_dict, key=distances_dict.get)
    print(
        f"""Minimal distance sentence for input string '{input_string}' is: '{minimal_distance_key}' with value {distances_dict[minimal_distance_key]}""")


def process_batch_mode():
    num_phrases = len(phrase_vector_dict)
    distance_matrix = np.zeros((num_phrases, num_phrases))
    phrases = list(phrase_vector_dict.keys())
    for i in range(num_phrases):
        for j in range(num_phrases):
            distance_matrix[i, j] = calculate_distance_of_vector_to_vector(
                phrase_vector_dict[phrases[i]],
                phrase_vector_dict[phrases[j]]
            )
    np.savetxt('data/matrix.csv', distance_matrix,
               delimiter='\t', fmt='%.6f')
