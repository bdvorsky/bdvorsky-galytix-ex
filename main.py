import argparse
from processing.processing_modes import process_batch_mode, process_on_the_fly_mode


def main():
    parser = argparse.ArgumentParser(description='Process input in batch or on-the-fly mode.')
    parser.add_argument('--batch', action='store_true', help='Path to the file for batch processing')
    parser.add_argument('--string', help='String for on-the-fly processing')

    args = parser.parse_args()

    if args.batch:
        process_batch_mode()
    elif args.string:
        process_on_the_fly_mode(args.string)
    else:
        print("Please provide either --file or --string argument.")


if __name__ == "__main__":
    main()
